(ns swapi-client.routes
  (:require [reitit.frontend :as reitit]))

(def router
  (reitit/router
   [["/" :index]
    ["/films" :films]
    ["/people" :people]
    ["/planets" :planets]]))

(defn path-for [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))
