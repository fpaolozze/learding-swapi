(ns swapi-client.people
  (:require [accountant.core :as accountant]
            [clojure.string :as string]
            [cljs.core.async :as async]
            [goog.events :as events]
            [reagent.core :refer [atom]]

            [swapi-client.resources :as resources :refer [map->Sort get-qs parse-sort]]
            [swapi-client.resources.chars :refer [arrow-char]]
            [swapi-client.resources.dom :refer [get-client-rect]]
            [swapi-client.resources.dom.table :refer [table-view]]
            [swapi-client.routes :refer [path-for]])
  (:import [goog.events EventType]))

(defonce people (atom nil))
(defonce columns-sort (atom []))
(defonce fields [[:id "ID"] [:name "Name"] [:birth_year "Birth Year"] [:eye_color "Eye Color"] [:gender "Gender"] [:hair_color "Hair Color"] [:height "Height"] [:mass "Mass"] [:skin_color "Skin Color"] [:homeworld_name "Homeworld"]])
(defonce sortable [:name :birth_year :eye_color :gender :hair_color :height :mass :skin_color :homeworld_name])
(defonce position (atom nil))

(defn get-people []
  (let [params {}
        params (if-let [qs-sort (parse-sort @columns-sort)]
                 (assoc params :sort qs-sort)
                 params)]
    (async/go
      (let [resp (async/<! (resources/get-data "people" :params params))]
        (reset! people (:body resp))))))

(defn remove-sort [field]
  (swap! columns-sort (fn [d] (into [] (remove #(= (:field %) field) d)))))

(defn find-sort [field]
  (first (filter #(= (:field %) (name field)) @columns-sort)))

(defn toggle-sort [field]
  (let [field (name field)
        dir (if-let [field-ref (find-sort field)] (case (:dir field-ref) 0 1 1 nil 0) 0)]
    (remove-sort field)

    (if dir
      (swap! columns-sort conj (map->Sort {:field field :dir dir}))))

  (get-people))

(defn gen-column [column-name title]
  (let [column-txt (str title (arrow-char (case (:dir (find-sort column-name)) 0 :up 1 :down nil)))
        opt-sort (if (some #{column-name} sortable) {:on-click #(toggle-sort column-name) :class "sortable"})]
    ^{:key column-name}
    [:th (into {} opt-sort) column-txt]))

(defn mouse-move-handler [offset]
  (fn [evt]
    (let [x (- (.-clientX evt) (:x offset))
          y (- (.-clientY evt) (:y offset))]
      (reset! position {:x x :y y}))))

(defn mouse-up-handler [on-move]
  (fn [evt]
    (events/unlisten js/window EventType.MOUSEMOVE on-move)))

(defn mouse-drag-handler [e]
  (let [{:keys [left top]} (get-client-rect e)
        offset {:x (- (.-clientX e) left)
                :y (- (.-clientY e) top)}
        on-move (mouse-move-handler offset)]
    (events/listen js/window EventType.MOUSEMOVE on-move)
    (events/listen js/window EventType.MOUSEUP (mouse-up-handler on-move))))

(defn index-page []
  (get-people)

  (fn []
    [:div {:style (if @position
                    {:position "absolute"
                     :left (str (:x @position) "px")
                     :top (str (:y @position) "px")})}
     [:div.window
      [:div.title-bar.draggable {:on-mouse-down mouse-drag-handler}
       [:div.title-bar-text "People"]
       [:div.title-bar-controls
        [:button {:aria-label "Close" :on-click #(accountant/navigate! (path-for :index))}]]]
      [:div {:class "window-body terminal"}
       [:p "PS C:\\Program Files\\Entities> List-People"]
       (if (not @people)
         [:p "Listing all people..."]
         [:table.items
          [:thead
           [:tr (doall (for [[n t] fields] [:th (gen-column n t)]))]
           [:tr (doall (for [[n t] fields] [:th (reduce str (repeat (count t) \-))]))]]
          [:tbody
           (for [person @people]
             ^{:key (:id person)}
             [:tr
              (for [[n t] fields]
                ^{:key (str n (:id person))}
                [:td (case n
                       :id [:a {:href (:url person)} (:id person)]
                       :homeworld_name [:a {:href (:homeworld_url person)} (:homeworld_name person)]
                       (get person n))])])]])]]]))
