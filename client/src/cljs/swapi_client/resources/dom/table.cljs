(ns swapi-client.resources.dom.table
  (:require [accountant.core :as accountant]

            [swapi-client.resources.chars :refer [arrow-char]]
            [swapi-client.routes :refer [path-for]]))

(defn table-view [title command-text placeholder columns body]
  [:div.window
   [:div.title-bar.draggable
    [:div.title-bar-text title]
    [:div.title-bar-controls
     [:button {:aria-label "Close" :on-click #(accountant/navigate! (path-for :index))}]]]
   [:div {:class "window-body terminal"}
    [:p (str "PS C:\\Program Files\\Entities> " command-text)]
    (if (not body)
      [:p placeholder]
      [:table.items
       [:thead
        (doall
         (for [[line-id line] columns]
           ^{:key (name line-id)} [:tr line]))]
       [:tbody body]])]])
