(ns swapi-client.resources.dom)

(defn get-client-rect [evt]
  (let [r (.getBoundingClientRect (.-target evt))]
    {:left (.-left r) :top (.-top r)}))
