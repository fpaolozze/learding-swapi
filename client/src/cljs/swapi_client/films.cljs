(ns swapi-client.films
  (:require [accountant.core :as accountant]
            [clojure.string :as string]
            [cljs.core.async :as async]
            [reagent.core :refer [atom]]

            [swapi-client.resources :as resources :refer [map->Sort get-qs parse-sort]]
            [swapi-client.resources.chars :refer [arrow-char]]
            [swapi-client.resources.dom.table :refer [table-view]]
            [swapi-client.routes :refer [path-for]]))

(defonce films (atom nil))
(defonce columns-sort (atom []))
(defonce fields [[:id "ID"] [:title "Title"] [:producer "Producer"] [:release_date "Release Date"] [:director "Director"] [:episode_id "Episode"] [:opening_crawl "Opening Crawl"]])
(defonce sortable [:title :producer :release_date :director :episode_id])

(defn get-films []
  (let [params {}
        params (if-let [qs-sort (parse-sort @columns-sort)]
                 (assoc params :sort qs-sort)
                 params)]
    (async/go
      (let [resp (async/<! (resources/get-data "films" :params params))]
        (reset! films (:body resp))))))

(defn remove-sort [field]
  (swap! columns-sort (fn [d] (into [] (remove #(= (:field %) field) d)))))

(defn find-sort [field]
  (first (filter #(= (:field %) (name field)) @columns-sort)))

(defn toggle-sort [field]
  (let [field (name field)
        dir (if-let [field-ref (find-sort field)] (case (:dir field-ref) 0 1 1 nil 0) 0)]
    (remove-sort field)

    (if dir
      (swap! columns-sort conj (map->Sort {:field field :dir dir}))))

  (get-films))

(defn gen-column [column-name title]
  (let [column-txt (str title (arrow-char (case (:dir (find-sort column-name)) 0 :up 1 :down nil)))
        opt-sort (if (some #{column-name} sortable) {:on-click #(toggle-sort column-name) :class "sortable"})]
    ^{:key column-name}
    [:th (into {} opt-sort) column-txt]))

(defn index-page []
  (get-films)

  (fn []
    (table-view
     "Films" "List-Films" "Listing all films..."
     [[:columns (doall (for [[n t] fields] (gen-column n t)))]
      [:separator (doall (for [[n t] fields] ^{:key n} [:th (reduce str (repeat (count t) \-))]))]]
     (if @films
       (for [film @films]
         ^{:key (:id film)}
         [:tr
          (for [[n t] fields]
            ^{:key (str n (:id film))}
            [:td (case n
                   :id [:a {:href (:url film)} (get film n)]
                   :opening_crawl [:div {:title (:opening_crawl film)} (if (> (count (:opening_crawl film)) 100) (str (subs (:opening_crawl film) 0 100) "...") (:opening_crawl film))]
                   (get film n))])])))))
