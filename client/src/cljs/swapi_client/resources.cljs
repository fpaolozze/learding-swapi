(ns swapi-client.resources
  (:require [cljs-http.client :as http]
            [cljs.core.async :as async]
            [clojure.string :as string]))

(defprotocol IQueryString
  (get-qs [this]))

(defrecord Sort [field dir]
  IQueryString
  (get-qs [this] (str field ":" dir)))

(defn parse-sort [sort-list]
  (not-empty (string/join ";" (map get-qs sort-list))))

(defn get-data [entity & {:keys [params]}]
  (http/get (str "http://localhost:8001/" entity) {:query-params params
                                                   :with-credentials? false}))
