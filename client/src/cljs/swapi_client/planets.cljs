(ns swapi-client.planets
  (:require [accountant.core :as accountant]
            [clojure.string :as string]
            [cljs.core.async :as async]
            [reagent.core :as reagent :refer [atom]]

            [swapi-client.resources :as resources :refer [map->Sort get-qs]]
            [swapi-client.resources.chars :refer [arrow-char]]
            [swapi-client.resources.dom.table :refer [table-view]]
            [swapi-client.routes :refer [path-for]]))

(defonce planets (atom nil))
(defonce columns-sort (atom []))
(defonce fields [[:id "ID"] [:name "Name"] [:surface_water "Surface Water"] [:climate "Climate"] [:orbital_period "Orbital Period"] [:diameter "Diameter"] [:gravity "Gravity"] [:population "Population"] [:terrain "Terrain"] [:rotation_period "Rotation Period"]])
(defonce sortable [:name :surface_water :climate :orbital_period :diameter :gravity :population :terrain :rotation_period])

(defn get-planets []
  (let [params {}
        params (if-let [qs-sort (not-empty (string/join ";" (map get-qs @columns-sort)))]
                 (assoc params :sort qs-sort)
                 params)]
    (async/go
      (let [resp (async/<! (resources/get-data "planets" :params params))]
        (reset! planets (:body resp))))))

(defn remove-sort [field]
  (swap! columns-sort (fn [d] (into [] (remove #(= (:field %) field) d)))))

(defn find-sort [field]
  (first (filter #(= (:field %) (name field)) @columns-sort)))

(defn toggle-sort [field]
  (let [field (name field)
        dir (if-let [field-ref (find-sort field)] (case (:dir field-ref) 0 1 1 nil 0) 0)]
    (remove-sort field)

    (if dir
      (swap! columns-sort conj (map->Sort {:field field :dir dir}))))

  (get-planets))

(defn gen-column [column-name title]
  (let [column-txt (str title (arrow-char (case (:dir (find-sort column-name)) 0 :up 1 :down nil)))
        opt-sort (if (some #{column-name} sortable) {:on-click #(toggle-sort column-name) :class "sortable"})]
    ^{:key column-name}
    [:th (into {} opt-sort) column-txt]))

(defn index-page []
  (get-planets)

  (fn []
    (table-view
     "Planets" "List-Planets" "Listing all planets..."
     [[:columns (doall (for [[n t] fields] (gen-column n t)))]
      [:separator (doall (for [[n t] fields] ^{:key n} [:th (reduce str (repeat (count t) \-))]))]]
     (if @planets
       (for [planet @planets]
         ^{:key (:id planet)}
         [:tr
          (for [[n t] fields]
            ^{:key (str n (:id planet))}
            [:td (case n
                   :id [:a {:href (:url planet)} (:id planet)]
                   (get planet n))])])))))
