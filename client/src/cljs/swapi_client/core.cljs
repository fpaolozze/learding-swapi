(ns swapi-client.core
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            [reagent.session :as session]
            [reitit.frontend :as reitit]
            [clerk.core :as clerk]
            [accountant.core :as accountant]

            [swapi-client.films :as films]
            [swapi-client.people :as people]
            [swapi-client.planets :as planets]
            [swapi-client.routes :refer [path-for router]]))

;; -------------------------
;; Page components

(defn home-page []
  (fn []
    [:span.main
     [:h1.center "STAR WARS"]]))

;; -------------------------
;; Translate routes -> page components

(defn page-for [route]
  (case route
    :index #'home-page
    :films #'films/index-page
    :people #'people/index-page
    :planets #'planets/index-page))

;; -------------------------
;; Page mounting component

(defn current-page []
  (fn []
    (let [page (:current-page (session/get :route))]
      [:div
       [:header
        [:div.menu
         [:ul
          (doall
           (for [[n t] '((:index "Home")
                         (:films "Films")
                         (:planets "Planets")
                         (:people "People"))]
             ^{:key n} [:li [:a {:href (path-for n)} [:img {:id (str "menu-" (name n)) :src "/images/img_trans.gif"}] [:p.menu t]]]))]]]
       [page]])))


;; -------------------------
;; Initialize app

(defn mount-root []
  (rdom/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (clerk/initialize!)
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (let [match (reitit/match-by-path router path)
            current-page (:name (:data  match))
            route-params (:path-params match)]
        (reagent/after-render clerk/after-render!)
        (session/put! :route {:current-page (page-for current-page)
                              :route-params route-params})
        (clerk/navigate-page! path)))
    :path-exists?
    (fn [path]
      (boolean (reitit/match-by-path router path)))})
  (accountant/dispatch-current!)
  (mount-root))
