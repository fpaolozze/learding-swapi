CREATE TABLE IF NOT EXISTS films (
  id INTEGER NOT NULL PRIMARY KEY,
  title TEXT NOT NULL,
  episode_id INTEGER NOT NULL,
  opening_crawl TEXT NOT NULL,
  director TEXT NOT NULL,
  producer TEXT NOT NULL,
  release_date DATE NOT NULL,
  url TEXT,
  created DATETIME,
  edited DATETIME
);

CREATE TABLE IF NOT EXISTS planets (
  id INTEGER NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  diameter INTEGER,
  rotation_period INTEGER,
  orbital_period INTEGER,
  gravity REAL,
  population INTEGER,
  climate TEXT,
  terrain TEXT,
  surface_water INTEGER,
  url TEXT,
  created DATETIME,
  edited DATETIME
);

CREATE TABLE IF NOT EXISTS people (
  id INTEGER NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  birth_year TEXT NOT NULL,
  eye_color TEXT NOT NULL,
  gender INTEGER NOT NULL DEFAULT 0, -- 0 -> Unknown, 1 -> Male, 2 -> Female, 3 -> N/A
  hair_color TEXT,
  height INTEGER,
  mass INTEGER,
  skin_color TEXT,
  homeworld INTEGER NOT NULL,
  url TEXT,
  created DATETIME,
  edited DATETIME,
  FOREIGN KEY(homeworld) REFERENCES planets(id)
);

CREATE TABLE IF NOT EXISTS species (
  id INTEGER NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  classification TEXT NOT NULL,
  designation TEXT,
  average_height INTEGER,
  average_lifespan INTEGER,
  eye_colors TEXT,
  hair_colors TEXT,
  skin_colors TEXT,
  language TEXT,
  homeworld INTEGER,
  url TEXT,
  created DATETIME,
  edited DATETIME,
  FOREIGN KEY(homeworld) REFERENCES planets(id)
);

CREATE TABLE IF NOT EXISTS starships (
  id INTEGER NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  model TEXT NOT NULL,
  starship_class TEXT NOT NULL,
  manufacturer TEXT,
  cost_in_credits INTEGER,
  length INTEGER,
  crew INTEGER NOT NULL,
  passengers INTEGER NOT NULL,
  max_atmosphering_speed INTEGER,
  hyperdrive_rating TEXT,
  MGLT INTEGER,
  cargo_capacity INTEGER,
  consumables TEXT,
  url TEXT,
  created DATETIME,
  edited DATETIME
);

CREATE TABLE IF NOT EXISTS vehicles (
  id INTEGER NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  model TEXT NOT NULL,
  vehicle_class TEXT NOT NULL,
  manufacturer TEXT,
  length INTEGER,
  cost_in_credits INTEGER,
  crew INTEGER NOT NULL,
  passengers INTEGER NOT NULL,
  max_atmosphering_speed INTEGER,
  cargo_capacity INTEGER,
  consumables TEXT,
  url TEXT,
  created DATETIME,
  edited DATETIME
);

CREATE TABLE IF NOT EXISTS people_films (
  people_id INTEGER NOT NULL,
  film_id INTEGER NOT NULL,
  FOREIGN KEY(people_id) REFERENCES people(id),
  FOREIGN KEY(film_id) REFERENCES film(id),
  UNIQUE(people_id, film_id)
);

CREATE TABLE IF NOT EXISTS people_species (
  people_id INTEGER NOT NULL,
  specie_id INTEGER NOT NULL,
  FOREIGN KEY(people_id) REFERENCES people(id),
  FOREIGN KEY(specie_id) REFERENCES species(id),
  UNIQUE(people_id, specie_id)
);

CREATE TABLE IF NOT EXISTS people_starships (
  people_id INTEGER NOT NULL,
  starship_id INTEGER NOT NULL,
  FOREIGN KEY(people_id) REFERENCES people(id),
  FOREIGN KEY(starship_id) REFERENCES starships(id),
  UNIQUE(people_id, starship_id)
);

CREATE TABLE IF NOT EXISTS people_vehicles (
  people_id INTEGER NOT NULL,
  vehicle_id INTEGER NOT NULL,
  FOREIGN KEY(people_id) REFERENCES people(id),
  FOREIGN KEY(vehicle_id) REFERENCES vehicles(id),
  UNIQUE(people_id, vehicle_id)
);

CREATE TABLE IF NOT EXISTS people_planets (
  people_id INTEGER NOT NULL,
  planet_id INTEGER NOT NULL,
  FOREIGN KEY(people_id) REFERENCES people(id),
  FOREIGN KEY(planet_id) REFERENCES planets(id),
  UNIQUE(people_id, planet_id)
);

CREATE TABLE IF NOT EXISTS films_species (
  film_id INTEGER NOT NULL,
  specie_id INTEGER NOT NULL,
  FOREIGN KEY(film_id) REFERENCES films(id),
  FOREIGN KEY(specie_id) REFERENCES species(id),
  UNIQUE(film_id, specie_id)
);

CREATE TABLE IF NOT EXISTS films_starships (
  film_id INTEGER NOT NULL,
  starship_id INTEGER NOT NULL,
  FOREIGN KEY(film_id) REFERENCES films(id),
  FOREIGN KEY(starship_id) REFERENCES starships(id),
  UNIQUE(film_id, starship_id)
);

CREATE TABLE IF NOT EXISTS films_vehicles (
  film_id INTEGER NOT NULL,
  vehicle_id INTEGER NOT NULL,
  FOREIGN KEY(film_id) REFERENCES films(id),
  FOREIGN KEY(vehicle_id) REFERENCES vehicles(id),
  UNIQUE(film_id, vehicle_id)
);

CREATE TABLE IF NOT EXISTS films_planets (
  film_id INTEGER NOT NULL,
  planet_id INTEGER NOT NULL,
  FOREIGN KEY(film_id) REFERENCES films(id),
  FOREIGN KEY(planet_id) REFERENCES planets(id),
  UNIQUE(film_id, planet_id)
);
