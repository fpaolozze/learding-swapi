(defproject swapi-server "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[cheshire "5.10.0"]
                 [clj-http "3.11.0"]
                 [compojure "1.6.2"]
                 [honeysql "1.0.444"]
                 [http-kit "2.5.0"]
                 [org.clojure/clojure "1.10.1"]
                 [org.clojure/java.jdbc "0.7.11"]
                 [org.xerial/sqlite-jdbc "3.34.0"]
                 [ring-cors "0.1.13"]
                 [ring/ring-devel "1.8.2"]]
  :main ^:skip-aot swapi-server.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
