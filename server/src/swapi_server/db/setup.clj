(ns swapi-server.db.setup
  (:require [clojure.java.io :as io]
            [clojure.java.jdbc :as jdbc]
            [clojure.string :as string])
  (:gen-class))

(defn run-setup [db-spec]
  (assert (io/resource "migrations/sql/setup.sql"))
  (loop [query (string/split (slurp (io/resource "migrations/sql/setup.sql")) #";\n")]
    (when (seq query)
      (jdbc/execute! db-spec (first query))
      (recur (rest query)))))

(defn init-folders []
  (let [db-folder (io/file "db")]
    (if-not (.exists db-folder)
      (.mkdirs db-folder))))
