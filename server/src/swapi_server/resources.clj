(ns swapi-server.resources
  (:require [honeysql.core :as sql-builder])
  (:gen-class))

(defn build-sql [sql-struct]
  (sql-builder/format sql-struct :namespace-as-table? true))
