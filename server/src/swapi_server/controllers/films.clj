(ns swapi-server.controllers.films
  (:require [swapi-server.db :as db]
            [swapi-server.models.films :as films])
  (:gen-class))

(defn get-all [columns-sort]
  (films/get-all (db/open-spec) columns-sort))

(defn get-one [id]
  (films/get-one (db/open-spec) id))
