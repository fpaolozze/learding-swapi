(ns swapi-server.controllers.planets
  (:require [swapi-server.db :as db]
            [swapi-server.models.planets :as planets])
  (:gen-class))

(defn get-all [columns-sort]
  (planets/get-all (db/open-spec) columns-sort))

(defn get-by-films [film-title]
  (planets/get-by-films (db/open-spec) film-title))

(defn get-one [id]
  (planets/get-one (db/open-spec) id))

  ;; people_planets
