(ns swapi-server.controllers.people
  (:require [swapi-server.db :as db]
            [swapi-server.models.people :as people])
  (:gen-class))

(defn get-all [columns-sort]
  (people/get-all (db/open-spec) columns-sort))

(defn get-one [id]
  (people/get-one (db/open-spec) id))
