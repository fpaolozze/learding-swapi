(ns swapi-server.crawler
  (:import java.text.SimpleDateFormat)
  (:require [cheshire.core :as cheshire]
            [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]
            [org.httpkit.client :as http]
            [swapi-server.db :as db])
  (:gen-class))

(defn extract-id [text]
  (if (not-empty text)
    (second (re-matches #"http://swapi.dev/api/[a-z]+/([0-9]+)/?" text))))

(defn- make-request [url]
  (let [{:keys [status body]} @(http/get url)
        body (cheshire/parse-string body true)]
    [(:results body) (:next body)]))

(defn get-all [url]
  (if url
    (let [[results next-page] (make-request url)]
      (lazy-cat results (get-all next-page)))))

(defn parse-double [v]
  (try
    (Double/parseDouble v)
    (catch NumberFormatException e)))

(defn parse-long [v]
  (try
    (Long/parseLong v)
    (catch NumberFormatException e)))

(defn parse-datetime [v]
  (-> (new SimpleDateFormat "yyyy-MM-dd'T'HH:mm:ss")
      (.parse v)))

(defn get-planets []
  (for [planet (get-all "https://swapi.dev/api/planets")]
    (let [planet (reduce #(update-in %1 (first %2) (second %2))
                         planet
                         [(list [:rotation_period] parse-long)
                          (list [:orbital_period] parse-long)
                          (list [:diameter] parse-long)
                          (list [:gravity] parse-double)
                          (list [:surface_water] parse-long)
                          (list [:population] parse-long)
                          (list [:created] parse-datetime)
                          (list [:edited] parse-datetime)])
          planet (assoc planet :id (extract-id (:url planet)))
          planet (update-in planet [:residents] (fn [o] (map extract-id o)))
          planet (update-in planet [:films] (fn [o] (map extract-id o)))]
      planet)))

(defn get-films []
  (for [film (get-all "https://swapi.dev/api/films")]
    (assoc film :id (extract-id (:url film)))))

(defn get-people []
  (for [people (get-all "https://swapi.dev/api/people")]
    (assoc people
           :id (extract-id (:url people))
           :homeworld (extract-id (:homeworld people)))))

(defn get-species []
  (for [specie (get-all "https://swapi.dev/api/species")]
    (assoc specie
           :id (extract-id (:url specie))
           :homeworld (extract-id (:homeworld specie)))))

(defn get-starships []
  (for [starship (get-all "https://swapi.dev/api/starships")]
    (assoc starship :id (extract-id (:url starship)))))

(defn get-vehicles []
  (for [vehicle (get-all "https://swapi.dev/api/vehicles")]
    (assoc vehicle :id (extract-id (:url vehicle)))))

(defn insert-stmt [table columns]
  (format "INSERT OR IGNORE INTO %s (%s) VALUES (%s);" (name table) (string/join "," (map name columns)) (string/join "," (repeat (count columns) \?))))

(defn cache-data [db-spec]
  (let [planets (get-planets)
        films (get-films)
        starships (get-starships)
        vehicles (get-vehicles)
        people (get-people)
        species (get-species)]

    (jdbc/with-db-transaction [trans db-spec]
      (println "Caching planets...")
      (doseq [planet (map #(dissoc % :residents :films) planets)]
        (let [stmt (insert-stmt "planets" (keys planet))]
          (jdbc/execute! trans (cons stmt (vals planet)))))

      (println "Caching films...")
      (doseq [film (map #(dissoc % :characters :planets :species :starships :vehicles) films)]
        (let [stmt (insert-stmt "films" (keys film))]
          (jdbc/execute! trans (cons stmt (vals film)))))

      (println "Caching starships...")
      (doseq [starship (map #(dissoc % :films :pilots) starships)]
        (let [stmt (insert-stmt "starships" (keys starship))]
          (jdbc/execute! trans (cons stmt (vals starship)))))

      (println "Caching vehicles...")
      (doseq [vehicle (map #(dissoc % :films :pilots) vehicles)]
        (let [stmt (insert-stmt "vehicles" (keys vehicle))]
          (jdbc/execute! trans (cons stmt (vals vehicle)))))

      (println "Caching people...")
      (doseq [people (map #(dissoc % :films :species :starships :vehicles) people)]
        (let [stmt (insert-stmt "people" (keys people))]
          (jdbc/execute! trans (cons stmt (vals people)))))

      (println "Caching species...")
      (doseq [specie (map #(dissoc % :people :films) species)]
        (let [stmt (insert-stmt "species" (keys specie))]
          (jdbc/execute! trans (cons stmt (vals specie)))))

      (println "Creating relation People X Films...")
      (doseq [person people]
        (doseq [film (:films person)]
          (let [stmt (insert-stmt "people_films" [:people_id :film_id])]
            (jdbc/execute! trans (cons stmt [(:id person) (extract-id film)])))))

      (println "Creating relation People X Species...")
      (doseq [person people]
        (doseq [specie (:species person)]
          (let [stmt (insert-stmt "people_species" [:people_id :specie_id])]
            (jdbc/execute! trans (cons stmt [(:id person) (extract-id specie)])))))

      (println "Creating relation People X Starships...")
      (doseq [person people]
        (doseq [starship (:starships person)]
          (let [stmt (insert-stmt "people_starships" [:people_id :starship_id])]
            (jdbc/execute! trans (cons stmt [(:id person) (extract-id starship)])))))

      (println "Creating relation People X Vehicles...")
      (doseq [person people]
        (doseq [vehicle (:vehicles person)]
          (let [stmt (insert-stmt "people_vehicles" [:people_id :vehicle_id])]
            (jdbc/execute! trans (cons stmt [(:id person) (extract-id vehicle)])))))

      (println "Creating relation People X Planets...")
      (doseq [person people]
        (let [planet (:homeworld person)]
          (let [stmt (insert-stmt "people_planets" [:people_id :planet_id])]
            (jdbc/execute! trans (cons stmt [(:id person) planet])))))

      (println "Creating relation Films X Species...")
      (doseq [film films]
        (doseq [specie (:species film)]
          (let [stmt (insert-stmt "films_species" [:film_id :specie_id])]
            (jdbc/execute! trans (cons stmt [(:id film) (extract-id specie)])))))

      (println "Creating relation Films X Starships...")
      (doseq [film films]
        (doseq [starship (:starships film)]
          (let [stmt (insert-stmt "films_starships" [:film_id :starship_id])]
            (jdbc/execute! trans (cons stmt [(:id film) (extract-id starship)])))))

      (println "Creating relation Films X Vehicles...")
      (doseq [film films]
        (doseq [vehicle (:vehicles film)]
          (let [stmt (insert-stmt "films_vehicles" [:film_id :vehicle_id])]
            (jdbc/execute! trans (cons stmt [(:id film) (extract-id vehicle)])))))

      (println "Creating relation Films X Planets...")
      (doseq [film films]
        (doseq [planet (:planets film)]
          (let [stmt (insert-stmt "films_planets" [:film_id :planet_id])]
            (jdbc/execute! trans (cons stmt [(:id film) (extract-id planet)]))))))))
