(ns swapi-server.db
  (:require [clojure.java.io :as io]
            [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]
            [swapi-server.db.setup :as setup])
  (:gen-class))

(defn upsert! [db table row where-clause]
  (jdbc/with-db-transaction [t-con db]
    (let [result (jdbc/update! t-con table row where-clause)]
      (if (zero? (first result))
        (jdbc/insert! t-con table row)
        result))))

(defn store-data [spec table rows]
  (loop [rows rows]
    (if (seq rows)
      (let [row (first rows)]
        (upsert! spec table row ["id = ?" (:id row)])
        (recur (rest rows))))))

(defn open-spec []
  (setup/init-folders)
  (let [sql-file (io/file "db/swapi.db")]
    (doto {:classname "org.sqlite.JDBC"
           :subprotocol "sqlite"
           :subname (.getAbsolutePath sql-file)}
      setup/run-setup)))
