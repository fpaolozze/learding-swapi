(ns swapi-server.models.people
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]

            [swapi-server.resources :refer [build-sql]])
  (:gen-class))

(defn get-all [db-spec columns-sort]
  (let [sql {:select [:p/* [:pl/name :homeworld_name] [:pl/url :homeworld_url]]
             :from [[:people :p]]
             :join [:people_planets [:= :p/id :people_id]
                    [:planets :pl] [:= :pl/id :planet_id]]
             :order-by (if columns-sort
                         (mapv (fn [[k v]] [(case k "homeworld_name" :pl/name (keyword "p" k)) (case v 0 :asc 1 :desc) :nulls-last]) columns-sort)
                         [[:p/id :asc]])}]
    (jdbc/query db-spec (build-sql sql))))

(defn get-one [db-spec id]
  (let [sql {:select [:*]
             :from [:people]
             :where [:= :id id]}]
    (first (jdbc/query db-spec (build-sql sql)))))
