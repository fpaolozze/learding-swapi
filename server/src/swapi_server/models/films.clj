(ns swapi-server.models.films
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]

            [swapi-server.resources :refer [build-sql]])
  (:gen-class))

(defn get-all [db-spec columns-sort]
  ^{:doc "List all films in database."}

  (let [sql {:select [:*]
             :from [:films]
             :order-by (if columns-sort
                         (mapv (fn [[k v]] [(keyword k) (case v 0 :asc 1 :desc) :nulls-last]) columns-sort)
                         [[:id :asc]])}]
    (jdbc/query db-spec (build-sql sql))))

(defn get-one [db-spec id]
  (let [sql {:select [:*]
             :from [:films]
             :where [:= :id id]}]
    (first (jdbc/query db-spec (build-sql sql)))))
