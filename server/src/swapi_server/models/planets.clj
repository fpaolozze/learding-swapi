(ns swapi-server.models.planets
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]

            [swapi-server.resources :refer [build-sql]])
  (:gen-class))

(defn get-all [db-spec columns-sort]
  (let [sql {:select [:*]
             :from [:planets]
             :order-by (if columns-sort
                         (mapv (fn [[k v]] [(keyword k) (case v 0 :asc 1 :desc) :nulls-last]) columns-sort)
                         [[:id :asc]])}]
    (jdbc/query db-spec (build-sql sql))))

(defn get-one [db-spec id]
  (let [sql {:select [:*]
             :from [:planets]
             :where [:= :id id]}]
    (first (jdbc/query db-spec (build-sql sql)))))

(defn get-by-films [db-spec film-title]
  (let [sql {:select [:p/* [:f/title :film_title]]
             :from [[:planets :p]]
             :join [:films_planets [:= :p/id :planet_id]
                    [:films :f] [:= :f/id :film_id]]
             :where [:like :f/title (str "%" film-title "%")]}]
    (jdbc/query db-spec (build-sql sql))))
