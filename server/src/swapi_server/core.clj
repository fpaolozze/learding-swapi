(ns swapi-server.core
  (:require [org.httpkit.server :as server]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.reload :refer [wrap-reload]]

            [swapi-server.db :as db]
            [swapi-server.crawler :as crawler]
            [swapi-server.routes :as routes])
  (:gen-class))

(defn run-crawler []
  (crawler/cache-data (db/open-spec)))

(defn -main [& args]
  (let [handler (-> #'routes/app-routes
                    wrap-reload
                    wrap-keyword-params
                    wrap-params
                    (wrap-cors :access-control-allow-origin [#".*"]
                               :access-control-allow-methods [:get]))]
    (server/run-server handler {:port 8001}))
  (println "Running server on port 8001!"))
