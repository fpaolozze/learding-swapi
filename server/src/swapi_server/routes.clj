(ns swapi-server.routes
  (:require [cheshire.core :as cheshire]
            [clojure.string :as string]
            [compojure.core :refer [defroutes routes GET]]
            [compojure.route :as route]

            [swapi-server.controllers.films :as films]
            [swapi-server.controllers.people :as people]
            [swapi-server.controllers.planets :as planets]
            [swapi-server.db :as db])
  (:gen-class))

(defn jsonify [handler]
  (fn [req]
    (-> (handler req)
        (assoc :headers {"Content-Type" "application/json"})
        (update-in [:body] cheshire/generate-string))))

(defn films-get-all [{:keys [params]}]
  (let [columns-sort (if (:sort params)
                       (for [[k v] (map #(string/split % #":") (string/split (:sort params) #";"))]
                         [k (Integer/parseInt v)]))]
    (films/get-all columns-sort)))

(defn films-get-one [id]
  (if-let [planet (films/get-one id)]
    {:status 200 :body planet}
    {:status 404 :body {:error (format "Planet id %s not found!" id)}}))

(defn people-get-all [{:keys [params]}]
  (let [columns-sort (if (:sort params)
                       (for [[k v] (map #(string/split % #":") (string/split (:sort params) #";"))]
                         [k (Integer/parseInt v)]))]
    (people/get-all columns-sort)))

(defn people-get-one [id]
  (if-let [person (people/get-one id)]
    {:status 200 :body person}
    {:status 404 :body {:error (format "Person id %s not found!" id)}}))

(defn planets-get-all [{:keys [params]}]
  (let [columns-sort (if (:sort params)
                       (for [[k v] (map #(string/split % #":") (string/split (:sort params) #";"))]
                         [k (Integer/parseInt v)]))]
    (planets/get-all columns-sort)))

(defn planets-get-one [id]
  (if-let [planet (planets/get-one id)]
    {:status 200 :body planet}
    {:status 404 :body {:error (format "Planet id %s not found!" id)}}))

(defroutes app-routes
  (jsonify
   (routes
    (GET "/films" [] films-get-all)
    (GET "/films/:id" [id] (films-get-one id))
    (GET "/people" [] people-get-all)
    (GET "/people/:id" [id] (people-get-one id))
    (GET "/planets" [] planets-get-all)
    (GET "/planets/:id" [id] (planets-get-one id))
    (GET "/planets-by-films/:film-title" [film-title] (planets/get-by-films film-title))
    (route/not-found {:status 404 :body {:error "Route not found!"}}))))
